package be.gato.cv.yolo;

import be.gato.io.IOFactory;
import be.gato.io.LinesReader;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.ImageWindow;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

/**
 * Project: OpenCv-YOLO
 * Package: be.gato.cv.yolo
 *
 * @author Gaston Lhoest
 * @version 1.0
 * @since 18/02/2018
 */
public class RunYolo {
    //static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    private static void idiotCheck() {
        System.out.println("OpenCv version: " + Core.VERSION);
        Mat m = new Mat(5, 10, CvType.CV_8UC1, new Scalar(0));
        System.out.println("OpenCV Mat: " + m);
        Mat mr1 = m.row(1);
        mr1.setTo(new Scalar(1));
        Mat mc5 = m.col(5);
        mc5.setTo(new Scalar(5));
        System.out.println("OpenCV Mat data:\n" + m.dump());
    }

    public static void main(String[] args) throws Exception {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        parseCommandLineArguments(args);

        //idiotCheck();

        runHardcodedYolo();
    }

    private static void parseCommandLineArguments(String[] args) {

    }

    private static ArrayList<String> readClassNameFile(String s) throws URISyntaxException {
        ArrayList<String> list = new ArrayList<>();
        Path p = Paths.get(s);
        try (LinesReader reader = IOFactory.createLinesReader(p)) {
            while (reader.isOpen() && reader.hasNext()) {
                String line = reader.readLine();
                if (line.trim().isEmpty()) continue;
                list.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static void runHardcodedYolo() throws Exception {
        Net net = Dnn.readNetFromDarknet("voc\\tiny-yolo-voc.cfg", "voc\\tiny-yolo-voc.weights");
        //Net net = Dnn.readNetFromDarknet("voc\\yolo-voc.cfg", "voc\\yolo-voc.weights");
        if (net.empty()) {
            System.err.println("can't load net");
            System.exit(-1);
        }
        double confidenceThreshold = 0.3;
        ArrayList<String> classNames = readClassNameFile("voc\\voc.names");
        
        detectOnImage(net,classNames,confidenceThreshold);
        //detectOnVideo(net,classNames,confidenceThreshold);
    }

    private static void detectOnVideo(Net net, ArrayList<String> classNames, double confidenceThreshold) throws InterruptedException {
        boolean stop = false;
        //VideoCapture vc = new VideoCapture("inputVideo.mp4");
        VideoCapture vc = new VideoCapture(0);
        Thread.sleep(1000);
        double fps = 24.0; //todo parse
        if (!vc.isOpened()){
            System.err.println("whut");
            System.exit(-3);
        }

        /* todo video file writing
        VideoWriter writer = new VideoWriter("videoTest.avi",-1,fps,new Size(vc.get(Videoio.CV_CAP_PROP_FRAME_WIDTH),vc.get(Videoio.CV_CAP_PROP_FRAME_HEIGHT)),true);
        if (!writer.isOpened()){
            System.err.println("whut");
            System.exit(-4);
        }
        */


        Mat frame = new Mat();
        JFrame window = new JFrame();
        JLabel label = new JLabel();
        ImageWindow imWindow = new ImageWindow("Output", frame);
        imWindow.setFrameLabelVisible(window, label);
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        imWindow.setMat(frame);

        long framecount = 1;
        long t1,t2;
        while (!stop){


            frame = new Mat();
            if (!vc.read(frame)) {
                System.out.println("End of file?");
                stop = true;
                continue;
            }
            t1 = System.currentTimeMillis();
            processImage(net, classNames, confidenceThreshold,frame);
            t2 = System.currentTimeMillis();

            /* todo video file writing
            if (writer.isOpened()){
                writer.write(frame);
                framecount++;
            }
            */



            //System.out.printf("\r frame: %6d time: %4dms",framecount++,(t2-t1)); //todo debug souf
            t1 = System.currentTimeMillis();
            label.setIcon(new ImageIcon(toBufferedImage(frame)));
            //imWindow.setMat(frame);
            window.pack();
            t2 = System.currentTimeMillis();
            //System.out.println(t2-t1); //todo debug sout
        }
        //writer.release();
        vc.release();
    }

    private static void processImage(Net net, ArrayList<String> classNames, double confidenceThreshold, Mat frame) {
        long t1,t2;
        t1 = System.currentTimeMillis();
        Random random = new Random();
        if (frame.empty()) {
            System.err.println("Image is empty");
            System.exit(-2);
        }

        if (frame.channels() == 4) {
            Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGRA2BGR);
        }

        Mat inBlob = Dnn.blobFromImage(frame, 1.0 / 255.0, new Size(416, 416), new Scalar(0), true, false);

        net.setInput(inBlob);

        t2 = System.currentTimeMillis();
        System.out.println("input set: "+(t2-t1)+"ms");
        t1 = System.currentTimeMillis();

        Mat detectionMat = net.forward("detection_out");

        t2 = System.currentTimeMillis();
        System.out.println("detecting: "+(t2-t1)+"ms");
        t1 = System.currentTimeMillis();

        for (int i = 0; i < detectionMat.rows(); i++) {
            int probabilityIndex = 5;
            for (int j = probabilityIndex; j < detectionMat.cols(); j++) {
                double[] detection = detectionMat.get(i, j);
                if (detection.length == 0) {
                    System.err.println("whut");
                } else if (detection.length == 1) {
                    if (detection[0] > confidenceThreshold) {
                        double x_center = detectionMat.get(i, 0)[0] * frame.cols();
                        double y_center = detectionMat.get(i, 1)[0] * frame.rows();
                        double width = detectionMat.get(i, 2)[0] * frame.cols();
                        double height = detectionMat.get(i, 3)[0] * frame.rows();
                        double confidence = detection[0] * 100;
                        Point p1 = new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2));
                        Point p2 = new Point(Math.round(x_center + width / 2), Math.round(y_center + height / 2));
                        Rect obj = new Rect(p1, p2);

                        Scalar object_roi_color = new Scalar(random.nextInt(256), random.nextInt(256), random.nextInt(256));

                        Imgproc.rectangle(frame, p1, p2, object_roi_color, 5);
                        String className = classNames.get(j - probabilityIndex);
                        String label = String.format("%s %.2f%%", className, confidence);
                        int[] baseline = new int[1];
                        Size labelSize = Imgproc.getTextSize(label, 0, 0.5, 1, baseline);
                        //todo draw rect around text
                        Imgproc.rectangle(frame, new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2)-(1.5*labelSize.height)), new Point(Math.round(x_center - width / 2) + labelSize.width, Math.round(y_center - height / 2)-(labelSize.height/2)), object_roi_color, 15);

                        Imgproc.putText(frame, label, new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2)-(labelSize.height/2)), 0, 0.5, new Scalar(0,0,0), 1);
                    }
                } else {
                    System.err.println("whut");
                }
            }
        }


        t2 = System.currentTimeMillis();
        System.out.println("filtering: "+(t2-t1)+"ms");

    }

    /**
     * Deprecated this is a hardcoded example
     * @param net
     * @param classNames
     * @param confidenceThreshold
     * @throws IOException
     * @throws URISyntaxException
     */
    @Deprecated
    private static void detectOnImage(Net net, ArrayList<String> classNames, double confidenceThreshold) throws IOException, URISyntaxException {
        Random random = new Random();
        Mat image = Imgcodecs.imread("horse dog.jpg");
        if (image.empty()) {
            System.err.println("Image is empty");
            System.exit(-2);
        }
        if (image.channels() == 4) {
            Imgproc.cvtColor(image, image, Imgproc.COLOR_BGRA2BGR);
        }
        Mat inBlob = Dnn.blobFromImage(image, 1.0 / 255.0, new Size(416, 416), new Scalar(0), true, false);

        net.setInput(inBlob);

        Mat detectionMat = net.forward("detection_out");

        for (int i = 0; i < detectionMat.rows(); i++) {
            StringBuilder sb = new StringBuilder();
            boolean det = false;
            int probabilityIndex = 5;

            sb.append(String.format("%10.0f ", detectionMat.get(i, 0)[0] * image.cols()));
            sb.append(String.format("%10.0f ", detectionMat.get(i, 1)[0] * image.rows()));
            sb.append(String.format("%10.0f ", detectionMat.get(i, 2)[0] * image.cols()));
            sb.append(String.format("%10.0f ", detectionMat.get(i, 3)[0] * image.rows()));
            sb.append(String.format("%4.2f ", detectionMat.get(i, 4)[0] * 100));
            for (int j = probabilityIndex; j < detectionMat.cols(); j++) {
                double[] detection = detectionMat.get(i, j);
                if (detection.length == 0) {
                    System.err.println("whut");
                } else if (detection.length == 1) {
                    //if (detection[0] > 0.00000000000001){
                    if (detection[0] > confidenceThreshold) {
                        sb.append(String.format("%4d:%10f ", j, detection[0]));
                        det = true;

                        double x_center = detectionMat.get(i, 0)[0] * image.cols();
                        double y_center = detectionMat.get(i, 1)[0] * image.rows();
                        double width = detectionMat.get(i, 2)[0] * image.cols();
                        double height = detectionMat.get(i, 3)[0] * image.rows();
                        double confidence = detection[0] * 100;
                        Point p1 = new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2));
                        Point p2 = new Point(Math.round(x_center + width / 2), Math.round(y_center + height / 2));
                        Rect obj = new Rect(p1, p2);

                        Scalar object_roi_color = new Scalar(random.nextInt(256), random.nextInt(256), random.nextInt(256));

                        Imgproc.rectangle(image, p1, p2, object_roi_color, 5);
                        String className = classNames.get(j - probabilityIndex);
                        String label = String.format("%s %.2f%%", className, confidence);
                        int[] baseline = new int[1];
                        Size labelSize = Imgproc.getTextSize(label, 0, 0.5, 1, baseline);
                        //todo draw rect around text
                        Imgproc.rectangle(image, new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2)-(1.5*labelSize.height)), new Point(Math.round(x_center - width / 2) + labelSize.width, Math.round(y_center - height / 2)-(labelSize.height/2)), object_roi_color, 15);

                        Imgproc.putText(image, label, new Point(Math.round(x_center - width / 2), Math.round(y_center - height / 2)-(labelSize.height/2)), 0, 0.5, new Scalar(0,0,0), 1);
                    }
                } else {
                    System.err.println("whut");
                    /*
                    for (double v : detection) {
                        sb.append(v+ ":");
                    }
                    sb.append("################################################# ");
                    det = true;
                    */
                }
            }
            if (det) {
                System.out.println(sb.toString());
            }
            //double confidence = detectionMat.t
        }
        JFrame frame = new JFrame();
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon(toBufferedImage(image)));
        ImageWindow window = new ImageWindow("Output", image);
        window.setFrameLabelVisible(frame, label);
        window.setMat(image);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        ImageIO.write(toBufferedImage(image),"jpg",new File("output.jpg"));
    }

    public static BufferedImage toBufferedImage(Mat m) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;

    }
}
